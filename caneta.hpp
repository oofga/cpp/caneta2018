#ifndef CANETA_HPP
#define CANETA_HPP

#include <string>

class Caneta {
   // Atributos
private:
   std::string cor;
   std::string marca;
   float preco;

   // Métodos
public:
   Caneta();  // Construtor
   ~Caneta(); // Destrutor
   // Métodos Acessores Get / Set
   void setCor(std::string cor);
   std::string getCor();
   void setMarca(std::string marca);
   std::string getMarca();
   void setPreco(float preco);
   float getPreco();
   // Outro Método
   void imprimeDados();

};


#endif
